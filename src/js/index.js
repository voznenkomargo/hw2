"use strict";

const burger = document.querySelector(".burger");
const menu = document.querySelector(".page-header__menu");
const burgerLine = Array.from(document.querySelectorAll(".burger__line"));
burger.addEventListener("click", toggleMenu);

function toggleMenu(e) {
   e.stopPropagation();
   menu.classList.toggle("active");
   burgerLine.forEach(element => {
      element.classList.toggle("active");
   });
}

document.onclick = function (event) {

   if (event.target.className != "menu__link") {
      menu.classList.remove("active");
      burgerLine.forEach(element => {
         element.classList.remove("active");
      });
   };
};